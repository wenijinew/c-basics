#include <stdio.h>

int main() {
    int    amount;
    printf("Give amount: ");
    scanf("%d", &amount);

    float  rate;
    printf("Give rate: ");
    scanf("%f", &rate);

    float result = amount * (1 + rate / 100);
    printf("\nAmount=SEK %d, rate=%.1f%% --> SEK %.2f\n",
           amount, rate, result);

    return 0;
}
