#pragma once

typedef enum {
    CHECK, CREDIT
} AccountType;

typedef struct {
    AccountType type;
    int         balance;
    float       rate;
} Account;


extern void accounts_init(Account accounts[], const unsigned int size);
extern Account* account_init(Account* acc, AccountType type, int balance, float rate);
extern void accounts_print(Account accounts[], const unsigned int size);
extern void accounts_update(Account accounts[], const unsigned int size);

