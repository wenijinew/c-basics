#include <stdio.h>
#include "account.h"

int main() {
    printf("Welcome to the C BANK\n");

    const int N = 12;
    Account accounts[N];
    accounts_init(accounts, N);
    accounts_print(accounts, N);

    printf("--- After account updates ---\n");
    for (int k = 0; k < N; ++k) {
        accounts[k].balance = account_amount(&accounts[k]);
    }
    accounts_print(accounts, N);

    return 0;
}
