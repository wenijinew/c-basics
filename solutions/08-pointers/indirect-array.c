#include <stdio.h>

int main() {
    int arr[] = {1, 2, 3, 4, 5, 6};
    const unsigned N = sizeof(arr) / sizeof(arr[0]);

    for (unsigned k = 0; k < N; ++k)
        printf("arr[%d] = %d\n", k, arr[k]);

    int* p = &arr[0];
    printf("*p = %d\n", *p);

    const int* END = &arr[N];
    for (p = &arr[0]; p != END; ++p)
        printf("*p = %d\n", *p);

    for (p = &arr[0]; p != END; ++p)  *p *= 10;

    for (p = &arr[0]; p != END; ++p)
        printf("*p = %d\n", *p);

    return 0;
}
