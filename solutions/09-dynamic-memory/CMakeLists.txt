cmake_minimum_required(VERSION 3.8)
project(09_dynamic_memory)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Werror -Wfatal-errors")

add_executable(bank
        Account.h Account.c
        Node.h Node.c
        List.h List.c
        bank.c
        )

