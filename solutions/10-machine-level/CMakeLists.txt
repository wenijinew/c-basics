cmake_minimum_required(VERSION 3.8)
project(10_machine_level)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "-Wall -Wextra ")

add_executable(bit-fun bit-fun.c)

