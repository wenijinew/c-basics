#pragma once

typedef struct  {
    char* name;
    int age;
} Person;


extern Person* person_alloc();
extern void person_dispose(Person* this);

extern Person* person_init(Person* this, const char* name, int age);
extern Person* person_finit(Person* this);

extern Person* person_new(const char* name, int age);
extern void person_delete(Person* this);


extern char* person_toString(Person* this);
extern char* person_getName(Person* this);
extern char* person_setName(Person* this, const char* newName);
extern int person_getAge(Person* this);
extern int person_setAge(Person* this, int newAge);
