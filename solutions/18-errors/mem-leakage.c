//COMPILE: c99 -g mem-leakage.c -o mem-leakage
//RUN    : valgrind --leak-check=yes ./mem-leakage [yes | no] [<int>]

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// --- type bool ---
typedef int bool;
const bool  false = 0;
const bool  true  = 1;

bool toBool(const char* s) {
    if (strcmp(s, "yes") == 0) return true;
    if (strcmp(s, "no") == 0) return false;
    return false;
}


// --- linked list ---
typedef struct sNode {
    int payload;
    struct sNode* next;
} Node;

Node* new_Node(int x, Node* next) {
    Node* this = (Node*) calloc(1, sizeof(Node));
    this->payload = x;
    this->next    = next;
    return this;
}


// --- app ---
void populate(Node** first, int length) {
    while (length-- > 0)
        *first = new_Node(length + 1, *first);
}

void print(Node* first) {
    for (Node* n = first; n != NULL; n = n->next)
        printf("%d ", n->payload);
    printf("\n");
}

void dispose(Node* first) {
    if (first != NULL) {
        printf("*** disposing all memory blocks\n");
        for (Node* nxt; first != NULL; first = nxt) {
            nxt = first->next;
            free(first);
        }
    }
}

// --- main ---
int main(int numArgs, char* args[]) {
    bool leak = (numArgs >= 2) ? toBool(args[1]) : true;
    int  N    = (numArgs >= 3) ? atoi(args[2]) : 10;
    printf("N = %d, leak=%s\n", N, (leak ? "YES" : "NO"));

    Node* first = NULL;
    populate(&first, N);
    print(first);

    if (leak) return 1;
    dispose(first);

    return 0;
}

