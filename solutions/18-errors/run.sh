#!/bin/bash

EXEDIR=./cmake-build-debug

echo 'Compiling...'
gcc -std=c11 -g -Wall -Wextra -Werror -Wfatal-errors ./mem-leakage.c -o $EXEDIR/mem-leakage

echo '----------------------------------------------------'
echo 'Running with leakage'
echo '----------------------------------------------------'
(
    set -x
    valgrind --leak-check=yes $EXEDIR/mem-leakage yes 42
)

echo '----------------------------------------------------'
echo 'Running without leakage'
echo '----------------------------------------------------'
(
    set -x
    valgrind --leak-check=yes $EXEDIR/mem-leakage no 42
)
